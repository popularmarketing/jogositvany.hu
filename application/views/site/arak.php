<?php include('header.php');?>
<?php include('primari.php');?>
            <!--======= BANNER =========-->
            <div class="sub-banner">
                <div class="container">
                    <h2>Ajánlataink</h2>
                    <ul class="links">
                        <li><a href="fooldal">Főoldal</a>/</li>
                        <li><a href="arak">Ajánlataink</a></li>
                    </ul>
                </div>
            </div>

            <!--======= CONTENT START =========-->
            <div class="content"> 

                <!--======= INTRESTED =========-->
                <section class="courses products-list">
                    <div class="container"> 
                        <!--======= TITTLE =========-->
                        <div class="tittle">
                            <h3>Ajánlataink</h3>
                            <p>Ajánlataink: B kategóriás jogsi 5 hét alatt! </p>
                            <hr>
                        </div>

                        <!--======= MONTH TITTLE =========-->
                        <div class="big-month"> KRESZ TANULÁSI LEHETŐSÉGEK, KÖLTSÉGEK </div>

                        <!--======= RODUCTS =========-->
                        <section class="products"> 

                            <!--======= PRODUCTS ROW =========-->
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="row">
										<?php foreach($ajanlatok->result() as $row){
													if($row->kategoria == 1){?>
											<!--======= ITEM 1 =========-->
											<li class="col-sm-6">
												<div class="prodct"> 

													<!--======= IMAGE =========-->                    
													<div class="col-md-6 no-padding"><img class="img-responsive" src="assets/uploads/termekek/<?php echo $row->fokep?>" alt=""></div>

													<!--======= PRODUCTS IMFO =========-->
													<div class="col-md-6 no-padding">
														<div class="pro-info"> 

															<!--======= ITEM Details =========--> 
															<?php print_r($row->leiras)?>
															<hr>
															<span class="price"><?php echo $row->ar?> Ft</span> <a href="jelentkezes" class="btn">JELENTKEZÉS</a></div>
													</div>
												</div>
											</li>
										<?php }}?>
                                    </ul>
                                </div>

                            </div>
                        </section>

                        <!--======= MONTH TITTLE =========-->
                        <div class="big-month"> VEZETÉSI KÖLTSÉGEK - B KATEGÓRIA </div>

                        <!--======= RODUCTS =========-->
                        <section class="products"> 

                            <!--======= PRODUCTS ROW =========-->
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="row">
										<?php foreach($ajanlatok->result() as $row){
													if($row->kategoria == 2){?>
											<!--======= ITEM 1 =========-->
											<li class="col-sm-6">
												<div class="prodct"> 

													<!--======= IMAGE =========-->                    
													<div class="col-md-6 no-padding"><img class="img-responsive" src="assets/uploads/termekek/<?php echo $row->fokep?>" alt=""></div>

													<!--======= PRODUCTS IMFO =========-->
													<div class="col-md-6 no-padding">
														<div class="pro-info"> 

															<!--======= ITEM Details =========--> 
															<?php print_r($row->leiras)?>
															<hr>
															<span class="price"><?php echo $row->ar?> Ft</span> <a href="jelentkezes" class="btn">JELENTKEZÉS</a></div>
													</div>
												</div>
											</li>
										<?php }}?>
                                    </ul>
                                </div>

                            </div>
                        </section>
						<!--======= MONTH TITTLE =========-->
                        <div class="big-month"> NORMÁL TANFOLYAM - ÖSSZKÖLTSÉGE EGYÉB KATEGÓRIÁKNÁL </div>

                        <!--======= RODUCTS =========-->
                        <section class="products"> 

                            <!--======= PRODUCTS ROW =========-->
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="row">
										<?php foreach($ajanlatok->result() as $row){
													if($row->kategoria == 3){?>
											<!--======= ITEM 1 =========-->
											<li class="col-sm-6">
												<div class="prodct"> 

													<!--======= IMAGE =========-->                    
													<div class="col-md-6 no-padding"><img class="img-responsive" src="assets/uploads/termekek/<?php echo $row->fokep?>" alt=""></div>

													<!--======= PRODUCTS IMFO =========-->
													<div class="col-md-6 no-padding">
														<div class="pro-info"> 

															<!--======= ITEM Details =========--> 
															<?php print_r($row->leiras)?>
															<hr>
															<span class="price"><?php echo $row->ar?> Ft</span> <a href="jelentkezes" class="btn">JELENTKEZÉS</a></div>
													</div>
												</div>
											</li>
										<?php }}?>
                                    </ul>
                                </div>

                            </div>
                        </section>
						<!--======= MONTH TITTLE =========-->
                        <div class="big-month"> S.O.S. TANFOLYAM - ÖSSZKÖLTSÉGE EGYÉB KATEGÓRIÁKNÁL </div>

                        <!--======= RODUCTS =========-->
                        <section class="products"> 

                            <!--======= PRODUCTS ROW =========-->
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="row">
										<?php foreach($ajanlatok->result() as $row){
													if($row->kategoria == 4){?>
											<!--======= ITEM 1 =========-->
											<li class="col-sm-6">
												<div class="prodct"> 

													<!--======= IMAGE =========-->                    
													<div class="col-md-6 no-padding"><img class="img-responsive" src="assets/uploads/termekek/<?php echo $row->fokep?>" alt=""></div>

													<!--======= PRODUCTS IMFO =========-->
													<div class="col-md-6 no-padding">
														<div class="pro-info"> 

															<!--======= ITEM Details =========--> 
															<?php print_r($row->leiras)?>
															<hr>
															<span class="price"><?php echo $row->ar?> Ft</span> <a href="jelentkezes" class="btn">JELENTKEZÉS</a></div>
													</div>
												</div>
											</li>
										<?php }}?>
                                    </ul>
                                </div>

                            </div>
                        </section>
                    </div>
                </section>
<?php include('footer.php');?>