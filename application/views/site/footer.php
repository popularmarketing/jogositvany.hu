<!--======= FOOTER =========-->
                <footer>
                    <div class="container">                       

                        <!--======= FOOTER ROWS =========-->                          
                        <ul class="row">

                            <!--======= ABOUT US =========-->
                            <li class="col-sm-6 col-md-4">
                                <h5>Rólunk</h5>
                                <hr>
                                <p>NON-STOP voltunk és 25 éves múltunk gyakorlatában az egész országban egyedül álló intenzív oktatási programot dolgoztunk ki. S.O.S. programunknak köszönhetően ma már ezrek vezetnek Európa útjain. Tartozz Te is közéjük. Keress minket! Mindenben segítségedre leszünk.</p>
                            </li>

                            <!--======= USEFUL LINKS =========-->
                            <li class="col-sm-6 col-md-4">
                                <h5>Hasznos linkek</h5>
                                <hr>
                                <ul class="link">
                                    <li><a href="fooldal"> Főoldal <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="arak"> Árak <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="jelentkezes"> Jelentkezés <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="kapcsolat"> Kapcsolat <i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </li>

                            <!--======= OPENING HOURS =========-->
                            <li class="col-sm-6 col-md-4">
                                <h5>Nyitvatartás</h5>
                                <hr>
                                <?php print_r($beallitasok->nyitvatartas)?>
                            </li>

                        </ul>

                        <div class="scroll-top">
                            <span class="fa fa-angle-up"></span>
                        </div> 
                    </div>

                    <!--======= RIGHTS =========-->
                    <div class="rights">
                        <div class="container">
                            <ul class="row">
                                <li class="col-md-8">
                                    <p>© 2016 Minden jog fenntartva | <a href="http://popularmarketing.hu">Weboldal készítés <img src="img/logo.png" height="50px"></a></p>
                                </li>
                                <!--======= SOCIAL ICON =========-->
                                <li class="col-md-4">
                                    <ul class="social_icons">
                                        <li class="facebook"><a href="https://www.facebook.com/nonstopautosiskola" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="js/jquery-1.11.0.min.js"></script> 
        <script src="js/wow.min.js"></script> 
        <script src="js/bootstrap.js"></script> 
        <script src="js/drive-me-plugin.js"></script> 
        <script src="js/jquery.cookie.js"></script> 
        <script src="js/jquery.isotope.min.js"></script> 
        <script src="js/jquery.flexslider-min.js"></script> 
        <script src="js/mapmarker.js"></script> 
        <script src="js/color-switcher.js"></script> 
        <script src="js/jquery.magnific-popup.min.js"></script> 
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/main.js"></script>
    </body>

<!-- Mirrored from event-theme.com/themes/driveme/drive-me/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Apr 2016 13:51:26 GMT -->
</html>