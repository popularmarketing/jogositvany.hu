<?php include('header.php');?>
<?php include('primari.php');?>
            <!--======= BANNER =========-->
            <div class="sub-banner">
                <div class="container">
                    <h2><?php echo $oldal->nev?></h2>
                    <ul class="links">
                        <li><a href="fooldal">Főoldal</a>/</li>
                        <li><a href="<?php echo $oldal->url?>"><?php echo $oldal->nev?></a></li>
                    </ul>
                </div>
            </div>

            <!--======= CONTENT START =========-->
            <div class="content"> 

                <!--======= INTRESTED =========-->
                <section class="courses">
                    <div class="container"> 

                        <!--======= RODUCTS =========-->
                        <section class="products prodct-single"> 

                            <!--======= PRODUCTS ROW =========-->
                            <div class="row"> 

                                <!--======= RIGHT SIDEBAR =========-->
                                <div class="col-sm-3" style="margin-top:0;"> 

                                    <!--======= Information =========-->
                                    <div class="where" style="margin-top:0;">
                                        <h6>Fontos információk <i class="fa fa-minus"></i></h6>
                                        <p> Email: <?php echo $beallitasok->nyilvanosemail?></p>
                                        <p> Vezetékes telefonunk: </br><?php echo $beallitasok->vezetekes?></p>
										<p> Mobil telefonunk: </br><?php echo $beallitasok->mobil?></p>
                                        <a href="kapcsolat">Kapcsolat</a> </div>
									<!--======= When & Where =========-->
                                    <div class="where">
                                        <h6>Hol & Mikor <i class="fa fa-minus"></i></h6>
                                        <p> <?php echo $beallitasok->uzletcim?></p>
                                        <p> Minden szombat 8:00</p>
                                        <a href="araink">Áraink</a> </div>
                                </div>
                                <div class="col-sm-9"> 
                                    <!--======= ITEM 1 =========-->
                                    <div class="container"> 
										<?php print_r($oldal->tartalom);?>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </section>

                <section class="quote">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10">
                                <h1>S.O.S. programunknak köszönhetően</h1>
                                <span>ma már ezrek vezetnek Európa útjain. Tartozz Te is közéjük! </span> </div>
                            <!--======= GET A QUOTE BUTTOn =========-->
                            <div class="col-md-2"> <a href="#." class="btn">JELENTKEZEM</a> </div>
                        </div>
                    </div>
                </section>

                <?php include('footer.php');?>