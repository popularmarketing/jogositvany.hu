<!--======= BANNER =========-->
            <div id="banner">
                <div class="flexslider">
                    <ul class="slides">
					<?php foreach($slider->result() as $row){?>
                        <!--======= SLIDE 2 =========-->
                        <li> <img src="assets/uploads/slider/<?php echo $row->file?>" alt="">
                            <div class="container">
                                <div class="text-slider">
                                    <div class="col-sm-7">
                                        <h3><?php echo $row->nev?> <i class="fa fa-long-arrow-right"></i></h3>
										<p><?php echo $row->url?></p>
                                    </div>
                                </div>
                            </div>
                        </li>
					<?php }?>
                    </ul>
                </div>
            </div>