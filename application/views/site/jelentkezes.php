<?php include('header.php');?>
<?php include('primari.php');?>
            <!--======= BANNER =========-->
            <div class="sub-banner">
                <div class="container">
                    <h2>Jelentkezés</h2>
                    <ul class="links">
                        <li><a href="fooldal">Főoldal</a>/</li>
                        <li><a href="JELENTKEZES">Jelentkezés</a></li>       
                    </ul>
                </div>
            </div>

            <!--======= CONTENT START =========-->
            <div class="content"> 

                <!--======= INTRESTED =========-->
                <section class="courses">
                    <div class="container"> 

                        <!--======= RODUCTS =========-->
                        <section class="products billing-info"> 

                            <!--======= PRODUCTS ROW =========-->
                            <div class="row">
                                <div class="col-md-9"> 

                                    <!--======= BILLING INFORMATION =========-->
                                    <div class="billing-tittle">
                                        <h5>Jelentkezés tanfolyamra</h5>
                                        <span class="process">01</span> </div>

                                    <!--======= FORM =========-->
                                    <div class="intres-lesson"> 

                                        <!--======= FORM =========-->
                                        <form id="user_billing" class="user-billing" action="oldal/jelentkezes" method="post">
                                            <ul class="row">

                                                <!--======= INPUT NAME =========-->
                                                <li class="col-sm-6">
                                                    <div class="form-group">
                                                        <input type="text" id="ub_phone" class="form-control" placeholder="Név" name="senderName">
                                                        <span class="fa fa-user"></span> </div>
                                                </li>

                                                <!--======= INPUT COURSE =========-->
                                                <li class="col-sm-6">
                                                    <div class="form-group">
                                                        <select type="text" class="form-control stylee" id="ub_course_name" placeholder="Tanfolyamok" name="tanfolyam">
															<?php foreach($tanfolyamok->result() as $row){?>
																<option value="<?php echo $row->nev;?>"><?php echo $row->nev;?></option>
															<?php }?>
                                                        </select>
                                                        <span class="fa fa-road"></span>
                                                    </div>
                                                </li>

                                                <!--======= INPUT PHONE NUMBER =========-->
                                                <li class="col-sm-6">
                                                    <div class="form-group">
                                                        <input type="text" id="ub_phone" class="form-control" placeholder="Telefonszám" name="phone">
                                                        <span class="fa fa-phone"></span> </div>
                                                </li>

                                                <!--======= INPUT PHONE NUMBER =========-->
                                                <li class="col-sm-6">
                                                    <div class="form-group">
                                                        <input type="text" id="ub_email" class="form-control" placeholder="Email" name="email">
                                                        <span class="fa fa-envelope"></span> </div>
                                                </li>

                                                <!--======= INPUT PHONE NUMBER =========-->
                                                <li class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea  rows="5" cols="" id="ub_message" placeholder="Megjegyzések" name="message"></textarea>
                                                        <span class="fa fa-file-text-o"></span> </div>
                                                </li>
												<li class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="submit" value="Jelentkezés" class="btn"> </div>
                                                </li>
                                            </ul>
                                        </form>
                                    </div>
                                </div>

                                <!--======= RIGHT SIDEBAR =========-->
                                <div class="col-md-3"> 

                                    <!--======= When & Where =========-->
                                    <div class="where" style="margin-top:0;">
                                        <h6>Kérdése van?<i class="fa fa-minus"></i></h6>
                                        <p>Hívjon minket bizalommal, tudunk segíteni!
                                            <?php echo $beallitasok->vezetekes?></p>
                                        <h3><?php echo $beallitasok->mobil?></h3>
                                        <a href="kapcsolat"><i class="fa fa-envelope"></i> Küldjön üzenetet!</a> </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </section>
<?php include('footer.php');?>