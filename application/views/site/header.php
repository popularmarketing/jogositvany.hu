<!DOCTYPE html>
<html>
    
<!-- Mirrored from event-theme.com/themes/driveme/drive-me/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Apr 2016 13:50:14 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title><?php echo $beallitasok->oldalnev?> | <?php echo $oldal->nev?></title>
        <meta name="keywords" content="HTML5,CSS3,HTML,Template,Multi-Purpose,jThemes Studio,Corporate Theme,Drive Me - Driving School Management HTML5 Theme" >
        <meta name="description" content="Drive Me - Driving School Management HTML5 Theme">
        <meta name="author" content="jThemes Studio">
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- FONTS ONLINE -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:500,900,300,700,400' rel='stylesheet' type='text/css'>

        <!--MAIN STYLE-->
        <link href="<?php echo base_url()?>/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url()?>/css/owl.carousel.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url()?>/css/main.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url()?>/css/style.css" rel="stylesheet" type="text/css">       
        <link href="<?php echo base_url()?>/css/responsive.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url()?>/css/animate.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url()?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">


        <!--======= COLOR STYLE CSS =========-->
        <link rel="stylesheet" id="color" href="<?php echo base_url()?>/css/color/default.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>