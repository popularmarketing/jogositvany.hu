    <body>
        <!--======= PRELOADER =========-->
        <div class="work-in-progress">
            <div id="preloader">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

        <!--======= WRAPPER =========-->
        <div id="wrap"> 

            <!--======= TOP BAR =========-->
            <div class="top-bar">
                <div class="container">
                    <ul class="left-bar-side">
                        <li> <a href="04_about_us.html" title="About DriveMe">Tudnivalók </a> - </li>
                        <li> <a href="blog.html" title="Blog">Árak </a> - </li>
                        <li> <a href="#.">Jelentkezés </a> - </li>
						<li> <a href="#.">Kapcsolat </a> - </li>
                        <li> <a href="https://www.facebook.com/nonstopautosiskola" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    </ul>
                    <ul class="right-bar-side">
                        <li> <a href="#."><i class="fa fa-phone"></i> <?php echo $beallitasok->mobil?> | <?php echo $beallitasok->vezetekes?> </a></li>
                        <li> <a href="#."><i class="fa fa-envelope"></i> <?php echo $beallitasok->nyilvanosemail?> </a></li>
                    </ul>
                </div>
            </div>

            <!--======= HEADER =========-->
            <header class="sticky">
                <div class="container">
                    <nav class="navbar navbar-default">
                        <div class="row">
                            <div class="navbar-header col-md-3 col-sm-3">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-res"> <span class="sr-only">Menü</span> <span class="fa fa-navicon"></span> </button>
                                <!--======= LOGO =========-->
                                <div class="logo"> <a href="fooldal"> <img src="assets/uploads/files/<?php echo $beallitasok->logo?>"> </a> </div>
                            </div>

                            <!--======= MENU =========-->
                            <div class="col-md-9 col-sm-9">
                                <div class="collapse navbar-collapse" id="nav-res">
                                    <ul class="nav navbar-nav">
									
										 <?php foreach ($primari as $row) { 
											$url = $row['cim'] == "" ? base_url("" . $row['url']) : $row['cim'];
											if (empty($row['gyerekek'])) {?>
												<li><a href="<?php echo $url?>"><?php echo $row['nev']?></a></li>
											<?php } else { ?>
												<li class="dropdown"> <a data-toggle="dropdown" class="dropdown-toggle"><?php echo $row['nev']?> <b class="caret"></b></a>
													<ul class="dropdown-menu animated-half fadeInUp">
														<?php foreach ($row['gyerekek'] as $gyRow) {
															$urlChild = $gyRow['cim'] == "" ? base_url("" . $gyRow['url']) : $gyRow['cim'];?>
															<li><a href="<?php echo $urlChild?>"><?php echo $gyRow['nev']?></a></li>
														<?php }?>														
													</ul>
												</li>                                        
											<?php }?>
										<?php }?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </header>
