<?php include('header.php');?>
<?php include('primari.php');?>
            <!--======= BANNER =========-->
            <div class="sub-banner">
                <div class="container">
                    <h2>Kapcsolat</h2>
                    <ul class="links">
                        <li><a href="fooldal">Főoldal</a>/</li>
                        <li><a href="kapcsolat">Kapcsolat</a></li>
                    </ul>
                </div>
            </div>

            <div class="content"> 

                <!--======= CONTACT =========-->
                <section class="contact"> 

                    <div class="container"> 

                        <!--======= CONTACT INFORMATION =========-->
                        <div class="contact-info">
                            <div class="row"> 

                                <!--======= CONTACT FORM =========-->
                                <div class="col-sm-6">
                                    <h3>lépjen velünk kapcsolatba</h3>
                                    <form role="form" id="contact_form" method="post" action="oldal/sendmail">
                                        <ul class="row">
                                            <li class="col-md-6">
                                                <input type="text" name="senderName" id="senderName" placeholder="Név">
                                            </li>
                                            <li class="col-md-6">
                                                <input type="text" name="email" id="email" placeholder="Email">
                                            </li>
                                            <li class="col-md-12">
                                                <textarea class="form-control" name="message" id="message" rows="5" placeholder="Üzenet"></textarea>
                                            </li>
                                            <li>
                                                <input type="submit" value="ÜZENET KÜLDÉSE" class="btn" width="100%">
                                            </li>
                                        </ul>
                                    </form>
                                </div>
                                <!--======= CONTACT =========-->
                                <div class="col-sm-6">
                                    <h3>elérhetőségek</h3>
                                    <ul class="con-det">

                                        <!--======= ADDRESS =========-->
                                        <li> <i class="fa fa-map-marker"></i>
                                            <h6>Üzletcím</h6>
                                            <p><?php echo $beallitasok->uzletcim?></p>
                                        </li>

                                        <!--======= EMAIL =========-->
                                        <li> <i class="fa fa-envelope"></i>
                                            <h6>email</h6>
                                            <p><?php echo $beallitasok->nyilvanosemail?></p>
                                        </li>

                                        <!--======= ADDRESS =========-->
                                        <li> <i class="fa fa-phone"></i>
                                            <h6>Telefonszámok</h6>
                                            <p><?php echo $beallitasok->mobil?></p>
                                            <p><?php echo $beallitasok->vezetekes?></p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <?php include('footer.php');?>