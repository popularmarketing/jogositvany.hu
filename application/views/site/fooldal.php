<?php include('header.php');?>
<?php include('primari.php');?>
<?php include('slider.php');?>
            <!--======= CONTENT START =========-->
            <div class="content">

                <!--======= FEATURE =========-->
                <section id="feature">
                    <div class="container"> 
						<?php print_r($oldal->tartalom);?>
                    </div>
                </section>

                <!--======= PRICING =========-->
                <section id="pricing">
                    <div class="container"> 
                        <!--======= TITTLE =========-->
                        <div class="tittle">
                            <h3>Legújabb ajánlataink</h3>
                            <p>Pár új ajánlatunk, amely hasznos lehet</p>
                            <hr>
                        </div>

                        <!--======= PRICING ROW =========-->

                        <ul class="row">
							<?php foreach($ajanlatok->result() as $row){?>
								<!--======= PRICE TABLE 1 =========-->
								<li class="col-sm-6 col-md-3">
									<div class="price-inner">
										<div class="price-head">
											<h4><?php echo $row->nev?></h4>
											<span ><h4 style="color: white; padding:0; margin-bottom:0;"><?php echo $row->ar?>Ft</h4></span> </div>
										<?php print_r($row->leiras);?>
										<p class="check-gr"><i class="fa fa-check-circle"></i> </p>
										<a href="jelentkezes" class="btn">Jelentkezés</a> </div>
								</li>
							<?php }?>
                        </ul>

                    </div>
                </section>

                <!--======= QUOTE =========-->
                <section class="quote">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10">
                                <h1>S.O.S. programunknak köszönhetően</h1>
                                <span>ma már ezrek vezetnek Európa útjain. Tartozz Te is közéjük! </span> </div>
                            <!--======= GET A QUOTE BUTTOn =========-->
                            <div class="col-md-2"> <a href="#." class="btn">JELENTKEZEM</a> </div>
                        </div>
                    </div>
                </section>

                <?php include('footer.php');?>