<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Usercheck extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function login($username, $password) {
        //create query to connect user login database
        $this->db->select('id, name, password,perm');
        $this->db->from('users');
        $this->db->where('name', $username);
        $this->db->where('password', MD5($password));
        $this->db->limit(1);
        //get query and processing
        $query = $this->db->get();
        if($query->num_rows() == 1) {

            $row = $query->row();
            $name = $row->name;
            $perm= $row->perm;

            $this->session->set_userdata('name', $name);
            $this->session->set_userdata('perm', $perm);

            return true;

        } else {
            return false; //if data is wrong
        }
    }
}