<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Alapfunction extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model("Lekerdezes");
    }

    function Nyelvek($option)
   {
        $nyelv[1]= array(
           'link' => "hu",
           'nev'  => "Magyar",
           'display'  => "Magyar",
           'icon' => "hu_flag.png"
       );
       $nyelv[2]= array(
           'link' => "en",
           'nev'  => "Angol",
           'display'  => "English",
           'icon' => "en_flag.png"
       );

       $nyelv[3]= array(
           'link' => "de",
           'nev'  => "Német",
           'display'  => "Deutsch",
           'icon' => "de_flag.png"
       );
       $nyelv[4]= array(
           'link' => "cz",
           'nev'  => "Cseh",
           'display'  => "Czeh",
           'icon' => "cz_flag.png"
       );

       if($option == "select")
       {
            foreach($nyelv as $sor)
            {
                $tomb[$sor['link']] = $sor['nev'];
            }
         return $tomb;
       }
       else
       { return $nyelv; }


   }

    function UrlGetek()
    {
           $return ="";
        if(count($_GET) > 0)
        {
            $get =  array();
            foreach($_GET as $key => $val)
            {
                $get[] = $key.'='.$val;
            }
            $return .= '?'.implode('&',$get);
        }
        return $return;
    }

    function page_to_admin()
    {
        $tomb[0] = "Szülő oldal";
        $query = $this->db->query("SELECT * FROM oldalak  order by nyelv,sorrend");

        foreach($query->result() as $sor)
        {
            $tomb[$sor->id] = $sor->nyelv." - ".$sor->nev;
        }

        return $tomb;
    } 
	
	function hirek_kategoria()
    {
       
        $query = $this->db->query("SELECT * FROM hirek_kategoria order by nyelv");

        foreach($query->result() as $sor)
        {
            $tomb[$sor->id] = $sor->nyelv." - ".$sor->nev;
        }

        return @$tomb;
    }
	
	function admins_to_select()
    {
       
        $query = $this->db->query("SELECT * FROM users");

        foreach($query->result() as $sor)
        {
            @$tomb[$sor->id] = $sor->publicname;
        }

        return $tomb;
    }

    function category_to_admin()
    {
        $tomb[0] = "Szülő kategória";
        $query = $this->db->query("SELECT * FROM kategoriak  order by nyelv,sorrend");

        foreach($query->result() as $sor)
        {
            $tomb[$sor->id] = $sor->nyelv." - ".$sor->nev;
        }

        return $tomb;
    }

    function tulajdonsag_kat_to_admin()
    {
        $tomb[0] = "";
        $query = $this->db->query("SELECT * FROM tulajdonsag_kat ");

        foreach($query->result() as $sor)
        {
            $tomb[$sor->id] = $sor->nyelv." - ".$sor->nev;
        }

        return $tomb;
    }

    function tulajdonsag_to_admin()
    {
        //Összeszedjük az összes tulajdonsag kategóriát és berakjuk a nevével együtt egy tömbbe
        $sql = $this->db->query("SELECT * FROM tulajdonsag_kat order by nyelv");
        foreach($sql->result() as $row)
        {
            $szulo[$row->id] = $row->nev;
        }

        $tomb[0] = "";
        $query = $this->db->query("SELECT * FROM tulajdonsag order by szulo, nyelv");
        foreach($query->result() as $sor)
        {

            $tomb[$sor->id] = $szulo[$sor->szulo]." (".$sor->nyelv.") - ".$sor->nev;

        }

        return $tomb;
    }

    function gyarto_to_admin()
    {
        $tomb = "";
        $query = $this->db->query("SELECT * FROM gyarto order by nyelv, sorrend");

        foreach($query->result() as $sor)
        {
            $tomb[$sor->id] = $sor->nyelv." - ".$sor->nev;
        }

        return $tomb;
    }

    function marka_to_admin()
    {   
        $tomb = "";
        $query = $this->db->query("SELECT * FROM marka order by nyelv, sorrend");

        foreach($query->result() as $sor)
        {
            $tomb[$sor->id] = $sor->nyelv." - ".$sor->nev;
        }

        return $tomb;
    }

    //Visszaadja a nyelveket és sessionbe rakja
    function nyelv($lang)
    {
        $controll = false;
        $nyelv = $this->nyelvek("");
        $session_nyelv = $this->session->userdata('nyelv');
        //Ha van get utasítás a nyelvre vonatkozólag
        if(!empty($lang))
        {

            for($i=1;$i<=count($nyelv);$i++)
            {
                //Ha egyezik a nyelv valamelyik ismert nyelvvel
                if($nyelv[$i]['link']== $lang)
                {
                    $controll = true;
                    $this->session->set_userdata('nyelv', $lang);
                    return $lang;
                }
            }
            if($controll!= true)
            {
                $lang = $nyelv[1]['link'];
                $this->session->set_userdata('nyelv', $lang);
                return $lang;
            }


        //Ha nincs get utasítás a nyelvre vonatkozólag
        }else
        {
            if(!empty($session_nyelv))
            {
                return $session_nyelv;
            }
            else{

                $nyelv = $nyelv[1]['link'];
                return $nyelv;
                }
        }


    }

    function site_data($lang,$type,$adat)
    {	
		
            $beallitasok = $this->Lekerdezes->beallitasok(" WHERE nyelv='".$lang."' ");
            $data['google_analytics'] = $beallitasok->google_analytics;
            $data['logo'] = $beallitasok->logo;

            //Ha van google analytics kód
            if(!empty($data['google_analytics']))
            {

            
				
				$data['google_analytics'] = "<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '".$data['google_analytics']."', 'auto');
  ga('send', 'pageview');

</script>";

             }

            //Ha a főoldalon vagyunk
            if($type == "home")
            {
                $data['title'] = $beallitasok->fooldal_title;
                $data['keywords'] = $beallitasok->fooldal_keywords;
                $data['description'] = $beallitasok->fooldal_description;

            } 
			
			//Ha egy aloldalon vagyunk
            if($type == "oldal" || $type = "hirek")
            {
				
                @$data['title'] = $adat->title;
                @$data['keywords'] = $adat->keywords;
                @$data['description'] = $adat->description;

            }
			
			
			
			if(empty($data['title'])){ $data['title'] = $beallitasok->fooldal_title; }
			if(empty($data['keywords'])){ $data['keywords'] = $beallitasok->fooldal_keywords; }
			if(empty($data['description'])){ $data['description'] = $beallitasok->fooldal_description; }
			if($type=="kategoria")
			{
				$data['title'] = "Ez egy title";
                $data['keywords'] = $beallitasok->fooldal_keywords;
                $data['description'] = $beallitasok->fooldal_description;
			}
			
		

        return $data;
    }

    function forditasok($lang)
    {
        $query = $this->db->query("SELECT * FROM nyelvi_forditasok WHERE nyelv = '$lang' ");
        foreach($query->result() as $sor)
        {
            if(empty($sor->file)){
            $data[$sor->azonosito] = $sor->ertek;
            }else{  $data[$sor->azonosito] = $sor->file; }
        }
        return $data;
    }

    public function shownotesadd($tartalom,$text,$ora,$perc,$mp,$id)
    {
        $tupdate = "<span clas='contenttime'>".$ora.":".$perc.":".$mp."</span> : ";
        $tupdate.= $text;
        $tupdate .="<hr>";
        $tupdate.= $tartalom;

        $data = array(
            'tartalom' => $tupdate
        );

        $this->db->where('id', $id);
        $this->db->update('hirek', $data);

        return $tupdate;

    }

    public function record_count_hirek($option = null) {
        $this->db->where("kategoria", $option);
        return $this->db->count_all("Hirek");
    }

    public function fetch_hirek($limit, $start, $option = null) {
        $this->db->where("kategoria", $option);
        $this->db->limit($limit, $start);
        $query = $this->db->get("Hirek");
        return $query;
   }
}