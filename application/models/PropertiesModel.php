<?php

class PropertiesModel extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function record_count($option = null) {
        
        if($option != null){
            return count($this->db->query($option)->result());            
        }else{
            
            $this->db->where("status","active");
            return $this->db->count_all("property");
        }
    }

    public function fetch_properties($limit, $start, $option = null) {
        $this->db->limit($limit, $start);
        
        if($option != null)
            $query = $this->db->query($option);
            else
        $query = $this->db->get("property");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
    
    public function get_property_by_guid($guid){
       
       $this->db->where("guid",$guid);
       return $this->db->get("property")->row();
       
   }
   
    public function get_property_image_guid($id){
        $this->db->where("propertyId",$id);
        $this->db->order_by("order", "asc"); 
       return $this->db->get("propertyimages")->result();
   }
   
    public function get_homepage_slider(){
        $this->db->order_by("id","asc");
        return $this->db->get("slider");
    }
    public function get_property_layout($id){
        $this->db->where("propertyId",$id);
        $this->db->order_by("order", "asc"); 
       return $this->db->get("propertylayout")->result();
   }
   
   public function get_property_extras_guid($id){
        $this->db->where("propertyId",$id);
       return $this->db->get("propertyextras")->result();
   }
   
   public function get_property_details_guid($id){
        $this->db->where("propertyId",$id);
       return $this->db->get("propertyplusdetails")->row();
   }
   
}
