<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oldal extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");


        $this->load->model("Lekerdezes");
        $this->load->model("Alapfunction");
        $this->load->helper("url");
        $this->load->library("pagination");

    }

    public function index()
    {
		$nyelv = $this->session->userdata('site_lang');
        $data['nyelv'] = $nyelv;
		$data['primari'] = $this->Lekerdezes->primari($data['nyelv']);
		$data['slider'] = $this->db->query("SELECT * FROM slider");
		$data['ajanlatok'] = $this->db->query("SELECT * FROM termekek ORDER BY id DESC limit 0,4");
		$data['beallitasok'] = $this->Lekerdezes->beallitasok();
		$data['oldal'] = $this->Lekerdezes->oldal('fooldal');
        $this->load->view("site/fooldal",$data);
    }

    public function url()
    {
		$url = $this->uri->segment(1);
        $nyelv = $this->session->userdata('site_lang');
        $data['nyelv'] = $nyelv;
		$data['primari'] = $this->Lekerdezes->primari($data['nyelv']);
		$data['beallitasok'] = $this->Lekerdezes->beallitasok();
		$data['oldal'] = $this->Lekerdezes->oldal($url);
        $this->load->view("site/oldal",$data);
    }
	public function spec()
    {
		$url = $this->uri->segment(1);
        $nyelv = $this->session->userdata('site_lang');
        $data['nyelv'] = $nyelv;
		$data['primari'] = $this->Lekerdezes->primari($data['nyelv']);
		$data['ajanlatok'] = $this->db->query("SELECT * FROM termekek");
		$data['tanfolyamok'] = $this->db->query("SELECT * FROM tanfolyamok");
		$data['beallitasok'] = $this->Lekerdezes->beallitasok();
		$data['oldal'] = $this->Lekerdezes->oldal($url);
        $this->load->view("site/".$url,$data);
    }
	public function sendmail()
     {
         
            $this->load->helper('url');
            
            $nyelv = $this->input->get('lang', TRUE);
            $data['nyelv'] = $this->Alapfunction->nyelv($nyelv);
            $beallitasok = $this->Lekerdezes->beallitasok(" WHERE nyelv='".$data['nyelv']."' ");
            
           
            $senderName = $this->input->post("senderName");
            $email = $this->input->post("email");
			$subject = "Email a jogosítvány.hu-ról";
            $message = $this->input->post("message");          
          
            $this->load->library('email');
         
            $this->email->from($email ,$senderName);
            $this->email->to($beallitasok->nyilvanosemail); 

            $this->email->subject($subject);
            $this->email->message($message);    

            $this->email->send();
            
            redirect($uri='./');

         
     }
	 public function jelentkezes()
     {
         
            $this->load->helper('url');
            
            $nyelv = $this->input->get('lang', TRUE);
            $data['nyelv'] = $this->Alapfunction->nyelv($nyelv);
            $beallitasok = $this->Lekerdezes->beallitasok(" WHERE nyelv='".$data['nyelv']."' ");
            
           
            $senderName = $this->input->post("senderName");
            $email = $this->input->post("email");
			$subject = "Jelentkezés egy tanfolyamra";
            $message = $this->input->post("message");    
			$phone = $this->input->post("phone");
			$tanfolyam = $this->input->post("tanfolyam");  
          
            $this->load->library('email');
         
            $this->email->from($email ,$senderName);
            $this->email->to($beallitasok->nyilvanosemail); 

            $this->email->subject($subject);
            $this->email->message("Jelentkezés a jogositvany.hu egyik tanfolyamára: Név: ".$senderName.", Tanfolyam: ".$tanfolyam.", Telefonszám: ".$phone.". Egyéb megjegyzések: ".$message);    

            $this->email->send();
            
            redirect($uri='./');

         
     }
}

